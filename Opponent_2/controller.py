# -*- coding: utf-8 -*-
import numpy
from .state import State
import random
import datetime, time

class Controller:
    def __init__(self, game, load, state):
        self.initialize_parameters(game, load, state)

    def initialize_parameters(self, game, load,state):
        self.state = state
        if load == None:
            self.parameters = numpy.random.normal(0, 1, 3*len(self.compute_features()))
        else:
            params = open(load, 'r')
            weights = params.read().split("\n")
            self.parameters = [float(x.strip()) for x in weights[0:-1]]


    def output(self, episode, performance):
       print "Performance do episodio #%d: %d" % (episode, performance)
       if episode > 0 and episode % 10 == 0:
           output = open("./params/%s.txt" % datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S'), "w+")
           for parameter in self.parameters:
               output.write(str(parameter) + "\n")

#--------------------------------------------------------------------------------------------------------

    #FUNCAO A SER COMPLETADA. Deve utilizar os pesos para calcular as funções de preferência Q para cada ação e retorna
    #-1 caso a ação desejada seja esquerda, +1 caso seja direita, e 0 caso seja ação nula
    def take_action(self, state):
        self.state = state
        features = self.compute_features()
        qualities = self.compute_qualities(features,self.parameters)
        action = numpy.argmax(qualities) - 1
        #print ">>"+str(action)+" "+str(qualities)
        return action

    #FUNCAO A SER COMPLETADA. Deve calcular features expandidas do estados (Dica: deve retornar um vetor)
    def compute_features(self):
        # Normalizando features
        angle = (self.state.rod_angle)/90.0
        angle_speed = self.state.angular_velocity
        pos_x = (self.state.wheel_x-600)/600.0
        wind = (self.state.wind)/500.0
        friction = self.state.friction
        speed_x = (self.state.velocity_x)/1000
        speed_y = self.state.velocity_y
        
        # print 'speedX ' + str(speed_x)
        # print 'speedY ' + str(speed_y)
        # print 'angle  ' + str(angle)     
        # print 'wind   ' + str(wind)
        # print 'x      ' + str(pos_x)

        return [((angle**2)*angle_speed)/(0.1+pos_x**2),pos_x/(0.1+speed_x**2),-angle,speed_x,pos_x,wind]

        # return [(angle**2)*angle_speed,pos_x,wind,(speed_x/100)*wind**5]
        # return [(angle**2)*angle_speed,((0.5*pos_x)-(pos_x**2)/2),wind]
        # return [(angle**2)*angle_speed,(((0.5*wind)-(wind**2)/2)+((0.5*pos_x)-(pos_x**2)/2))+(speed_x/100.0)**2]        
        # return [(angle**2)*angle_speed,(((0.5*wind)-(wind**2)/2)+((0.5*pos_x)-(pos_x**2)/2))+(speed_x/100.0)**2]        

        # return [(angle**2)*angle_speed,((300*pos_x)-(pos_x**2)/2), angle*(angle_speed**2)]

        # return [(angle**2)*angle_speed,((300*pos_x)-(pos_x**2)/2), angle*(angle_speed**2),(wind-speed_x+(pos_x-600))**2]
        # return [(angle**2)*angle_speed/(1+((wind-speed_x+(pos_x-600))**2)),((300*pos_x)-(pos_x**2)/2), angle*(angle_speed**2)]
        # return [(angle**2)*angle_speed,((300*pos_x)-(pos_x**2)/2)*(wind+1)/((speed_x**2)+1), angle*(angle_speed**2)]
        # return [(angle**2)*angle_speed,((300*pos_x)-(pos_x**2)/2), angle*(angle_speed**2),(((wind**2)/2)-((500**2)*wind))*(((pos_x**3)/3)-((600**3)*pos_x))+((300*pos_x)-(pos_x**2)/2)]
        #return [(angle**2)*angle_speed,((300*pos_x)-(pos_x**2)/2), angle*(angle_speed**2),(((wind**3)/3)-(250000*wind))/(((300*pos_x)-(pos_x**2)/2))]


    tries = []
    triesCounter = 0
    bestParams = []
    bestPerformance = 0
    #FUNCAO A SER COMPLETADA. Deve atualizar a propriedade self.parameters
    def update(self,episode,performance):        
        print self.state.wind

        if(performance == 20000):
            print self.parameters

        self.simulated_annealing(performance,episode,100) # FIXME: chamar HC ou AG quando i>100
        # self.hill_climbing(performance)
        # self.ascent_gradient(performance)

        return 

    def compute_qualities(self,features,params):
        featuresSize = len(features)

        left = numpy.multiply(features, params[0:featuresSize])
        none = numpy.multiply(features, params[featuresSize:2*featuresSize])
        right = numpy.multiply(features, params[2*featuresSize:3*featuresSize])
        
        left = numpy.sum(left)
        none = numpy.sum(none)
        right = numpy.sum(right)

        return left,none,right

    def compute_gradient(self,features):
        angle = self.state.rod_angle
        angle_speed = self.state.angular_velocity
        pos_x = self.state.wheel_x

        gradient = [-1] * len(self.compute_features())
        gradient[0] = 2*angle*angle_speed + (angle**2)
        gradient[1] = 600-pos_x
        gradient[2] = 2*angle_speed*angle + (angle_speed**2)

        return gradient

    def ascent_gradient(self,performance):
        features = self.compute_features()

        gradient = self.compute_gradient(features)
        featuresSize = len(gradient)
        # print gradient
        alpha = [0.45]*featuresSize

        self.parameters[0:featuresSize] = numpy.sum([self.parameters[0:featuresSize], numpy.multiply(alpha, gradient)],axis=0)            
        self.parameters[featuresSize:2*featuresSize] = numpy.sum([self.parameters[featuresSize:2*featuresSize], numpy.multiply(alpha, gradient)],axis=0)
        self.parameters[2*featuresSize:3*featuresSize] = numpy.sum([ self.parameters[2*featuresSize:3*featuresSize], numpy.multiply(alpha,gradient)],axis=0)
        

        if self.triesCounter == 0 or self.triesCounter > 5:
            if self.triesCounter > 5:
                self.parameters = self.bestParams
            params = self.parameters
            self.tries = []
            self.triesCounter = 0
            for i in range(0,6):
                step = numpy.random.normal(0,10,len(params))  
                params[0:featuresSize] = numpy.sum([self.parameters[0:featuresSize], numpy.multiply(alpha, gradient)],axis=0)            
                params[featuresSize:2*featuresSize] = numpy.sum([self.parameters[featuresSize:2*featuresSize], numpy.multiply(alpha, gradient)],axis=0)
                params[2*featuresSize:3*featuresSize] = numpy.sum([ self.parameters[2*featuresSize:3*featuresSize], numpy.multiply(alpha,gradient)],axis=0)

                self.tries.append(numpy.sum([step,params],axis=0))
            #print self.tries
    
        if performance > self.bestPerformance:
            self.bestParams = self.parameters
            self.bestPerformance = performance
            #print "BEST!! parameters:"  + str(self.bestParams)
        
        self.triesCounter += 1
        if(performance == 20000):
            self.parameters = self.bestParams
            self.triesCounter = 0
        else:
            self.parameters = self.tries[self.triesCounter - 1]

        return

    def simulated_annealing(self,performance,heat,max_heat):       
        T = (max_heat-heat)
        if self.triesCounter == 0 or self.triesCounter > 5:
            self.triesCounter = 0
            params = self.parameters
            features = self.compute_features()                       
            self.tries = []
            for i in range(0,6):
                random = numpy.random.normal(0,1,len(params))                          
                self.tries.append(random)
        delta = performance - self.bestPerformance
        randomRate = numpy.random.ranf()
        exp = numpy.exp(delta/T)
        #print exp
        if delta > 0:
            self.bestPerformance = performance
            self.bestParams = self.parameters
            #print "BEST!! parameters:"  + str(self.bestParams)
        elif exp > 0.005:
            self.bestParams = self.parameters
            #print "Strange value accepted!! parameters:"  + str(self.bestParams)
        if T <= 1:
            self.parameters = self.bestParams
        self.triesCounter += 1

        if(performance == 20000):
            self.parameters = self.bestParams
            self.triesCounter = 0
        else:
            self.parameters = self.tries[self.triesCounter - 1]
        return

    def hill_climbing(self,performance):
        if self.triesCounter == 0 or self.triesCounter > 5:
            if self.triesCounter > 5:
                self.parameters = self.bestParams
            params = self.parameters
            self.tries = []
            self.triesCounter = 0
            for i in range(0,6):
                step = numpy.random.normal(0,10,len(params))            
                self.tries.append(numpy.sum([step,params],axis=0))
    
        if performance > self.bestPerformance:
            self.bestParams = self.parameters
            self.bestPerformance = performance
            #print "BEST!! parameters:"  + str(self.bestParams)
    
        self.triesCounter += 1

        if(performance == 20000):
            self.parameters = self.bestParams
            self.triesCounter = 0
        else:
            self.parameters = self.tries[self.triesCounter - 1]
        return
